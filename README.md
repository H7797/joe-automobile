Joe’s Automotive performs the following routine maintenance services:
• Oil change - $26
• Lube job - $18
• Radiator flush - $30
• Transmission flush - $80
• Inspection - $15
• Muffler replacement - $100
• Tire rotation - $20

Joe also performs other nonroutine services and charges for parts and labor ($20 per hour). Create an
application that displays the total for a customer’s visit to Joe’s.
The application should have the following value-returning methods:



• OilLubeCharges – Returns the total charges for an oil change and/or a lube job, if any.
• FlushCharges – Returns the total charges for a radiator flush and/or a transmission flush, if any.
• MiscCharges – Returns the total charges for an inspection, muffler replacement, and/or a tire
replacement, if any.
• OtherCharges – Returns the total charges for other services (parts and labor, if any)
• TaxCharges – Returns the amount of sales tax, if any. Sales tax is 6% and is charged only on
parts.
• TotalCharges – Returns the total charges

The application should have the following void methods called when the user clicks the Clear button:
• ClearOilLube – clears the UI for selecting oil change and lube job.
• ClearFlushes – clears the UI for selecting radiator and transmission flush.
• ClearMisc – clears the UI for inspection, muffler replacement, and tire rotation.
• ClearOther – clears the UI collecting user data for parts and labor
• ClearTaxAndTotal – clears the tax and total area of the form.


**How to build and install project:

To use this project install Visual Studio 2018/2019.

open .sln file into visual studio.build the program and run it.

1) user have to choose service according to requirments.
2) how many hours labour has worked and what parts are used in services.
3) calculated it with tax
4) display the final amount to the user.
..




**Edit a file, create a new file, and clone from Bitbucket in under 2 minutes**

When you're done, you can delete the content in this README and update the file with details for others getting started with your repository.

*We recommend that you open this README in another tab as you perform the tasks below. You can [watch our video](https://youtu.be/0ocf7u76WSo) for a full demo of all the steps in this tutorial. Open the video in a new tab to avoid leaving Bitbucket.*



## Edit a file

You’ll start by editing this README file to learn how to edit a file in Bitbucket.

1. Click **Source** on the left side.
2. Click the README.md link from the list of files.
3. Click the **Edit** button.
4. Delete the following text: 
5. After making your change, click **Commit** and then **Commit** again in the dialog. The commit page will open and you’ll see the change you just made.
6. Go back to the **Source** page.



## Create a file

Next, you’ll add a new file to this repository.

1. Click the **New file** button at the top of the **Source** page.
2. Give the file a filename of **contributors.txt**.
3. Enter your name in the empty file space.
4. Click **Commit** and then **Commit** again in the dialog.
5. Go back to the **Source** page.

Before you move on, go ahead and explore the repository. You've already seen the **Source** page, but check out the **Commits**, **Branches**, and **Settings** pages.
