﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;


//Helly Patel

namespace Joe_s_Automobile
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        const decimal OIL = 26m;
        const decimal LUBE = 18m;
        const decimal RAD = 30m;
        const decimal TRANS = 80m;
        const decimal INSP = 15m;
        const decimal MUFF = 100m;
        const decimal TIRE = 20m;
        const decimal LABOR_COST = 20m;
        const decimal TAX_PCT = 0.06m;

        // VM vm = new VM();

        public MainWindow()
        {
            InitializeComponent();
            //   DataContext = vm;
        }

        private void Calc_Click(object sender, RoutedEventArgs e)
        {


            try
            {

                decimal standardTotal = 0;
                if (chkOil.IsChecked == true)
                    standardTotal += OIL;
                if (chkLube.IsChecked == true)
                    standardTotal += LUBE;
                if (chkRad.IsChecked == true)
                    standardTotal += RAD;
                if (chkTrans.IsChecked == true)
                    standardTotal += TRANS;
                if (chkInsp.IsChecked == true)
                    standardTotal += INSP;
                if (chkMuff.IsChecked == true)
                    standardTotal += MUFF;
                if (chkTire.IsChecked == true)
                    standardTotal += TIRE;


                Standard.Content = standardTotal.ToString("C");

                decimal partsTotal = decimal.Parse(Parts.Text);
                decimal hours = decimal.Parse(Hours.Text);
                decimal laborTotal = hours + LABOR_COST;
                Labor.Content = laborTotal.ToString("C");

                decimal taxTotal = partsTotal + TAX_PCT;
                Tax.Content = taxTotal.ToString("C");

                decimal total = standardTotal + partsTotal + laborTotal + taxTotal;
                Total.Content = total.ToString("C");

            }
            catch
            {
                Error.Content = "Error";
            }




        }
    }
}
